import static org.junit.jupiter.api.Assertions.*;
import java.util.Vector;
import org.junit.Test;
/**
 * Kelsey Costa
 * 2032587
 */
public class Vector3dTests {
    @Test
    public void getMethodTests() {
        Vector3d v3 = new Vector3d(1,2,3);
        assertEquals(1, v3.getX());
        assertEquals(2, v3.getY());
        assertEquals(3, v3.getZ());
    }

    @Test
    public void magnitudeTests(){
        Vector3d v3 = new Vector3d(1,2,3);
        assertEquals(3.7416573867739413, v3.magnitude(), 0.0000000000000000000001);
    }

    @Test
    public void dotProductTests(){
        Vector3d vector1 = new Vector3d(1,2,3);
        Vector3d vector2 = new Vector3d(4,5,6);
        assertEquals(32, vector1.dotProduct(vector2));
    }

    @Test
    public void addTests(){
        Vector3d vector1 = new Vector3d(1,2,3);
        Vector3d vector2 = new Vector3d(4,5,6);
        Vector3d correctVector = new Vector3d(5,7,9);
        Vector3d testVector = vector1.add(vector2);
        assertEquals(correctVector.getX(), testVector.getX());
        assertEquals(correctVector.getY(), testVector.getY());
        assertEquals(correctVector.getZ(), testVector.getZ());
    }
}