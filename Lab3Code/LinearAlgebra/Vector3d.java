/**
 * Kelsey Costa
 * 2032587
 */

public class Vector3d{
    private double x;
    private double y;
    private double z;

    //constructor
    public Vector3d(double x, double y, double z){
        this.x = x;
        this.y = y;
        this.z = z;
    }

    //get methods
    public double getX(){
        return this.x;
    }
    public double getY(){
        return this.y;
    }
    public double getZ(){
        return this.z;
    }

    //returns the magnitude of a 3d vector
    public double magnitude(){
        return Math.sqrt(Math.pow(this.x, 2) + Math.pow(this.y, 2) + Math.pow(this.z, 2));
    }

    //returns the dot product of current vector3d and a given vector 3d
    public double dotProduct(Vector3d exterVector){
        return (this.x * exterVector.getX()) + (this.y * exterVector.getY()) + (this.z * exterVector.getZ());
    }

    //adds a given vector3d to current vector3d and returns a new vector 
    public Vector3d add(Vector3d exterVector){
        Vector3d v3 = new Vector3d(this.x + exterVector.getX(), this.y + exterVector.getY(), this.z + exterVector.getZ());
        return v3;
    }

    public String toString(){
        return "x: " + this.x + " y: " + this.y + " z: " + this.z;
    }
}